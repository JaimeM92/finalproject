﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitApp : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        // Quit Game Application
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}

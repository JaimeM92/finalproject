﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Header("Camera Stats")]
    public Transform player;
    public float CamSpeed;
    public Vector3 offset;
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Vector3 campPosition = player.position + offset;

        Vector3 smoothPos = Vector3.Lerp(transform.position, campPosition, CamSpeed);
        transform.position = smoothPos;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Player movement stats
    [Header("Player Movement")]
    public float playerSpeed;
    private float moveInput;
    private bool moveRight = true;
    [Space(5)]

    // player Jump stats
    [Header("Player Jump Stats")]
    public float jumpForce;
    public int maxJump;
    private int extraJump;
    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatisGround;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        extraJump = maxJump;
	}


    // Update is called once per frame
    void Update ()
    {
        // Creates an object to check if grounded
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatisGround);

        // Call to the move input
        moveInput = Input.GetAxis("Horizontal");

        // Move player
        rb.velocity = new Vector2(moveInput * playerSpeed, rb.velocity.y);

        // Flip player 
        if (moveRight == false && moveInput > 0)
        {
            Flip();
        }

        // Flip player
        else if (moveRight == true && moveInput < 0)
        {
            Flip();
        }

        // Play animation with input
        if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
        {
            GetComponent<Animator>().Play("DinoWalk");
        }
        
        // Check if player is gorunded
        if (isGrounded == true)
        {
            extraJump = maxJump;
        }

        // Player has no used jumps
        if (Input.GetKeyDown(KeyCode.Space) && extraJump > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extraJump--;
            GetComponent<Animator>().Play("DinoJump");
        }

        // player has used jumps
        else if(Input.GetKeyDown(KeyCode.Space) && extraJump == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
            GetComponent<Animator>().Play("DinoJump");
        }

    }

    // Function to flip player
    void Flip()
    {
        moveRight = !moveRight;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBckgnd : MonoBehaviour
{
    // Stats to movebackground with camera
    private Transform cameraTransform;
    private Transform[] layers;
    private float viewZone = 10;
    private int leftIndex;
    private int rightIndex;
    private float lastCameraX;

    public float backGroundSize;
    public float parallaxSpeed;

	// Use this for initialization
	void Start ()
    {
        cameraTransform = Camera.main.transform;
        lastCameraX = cameraTransform.position.x;
        layers = new Transform[transform.childCount];

        // Go through the different child objects within the parent background object
        for (int i = 0; i < transform.childCount; i++)
        {
            layers[i] = transform.GetChild(i);  
        }

        leftIndex = 0;
        rightIndex = layers.Length - 1;
    }


    // Update is called once per frame
    void Update()
    {
        float deltaX = cameraTransform.position.x - lastCameraX;
        transform.position += Vector3.right * (deltaX * parallaxSpeed);
        lastCameraX = cameraTransform.position.x;

        // when camera moves, scroll
        if (cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone))
        {
            ScrollLeft();
        }

        // When camera moves, scroll
        if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone))
        {
            ScrollRight();
        }
    }

    void ScrollLeft()
    {
        // When moving right scroll left
        layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - backGroundSize);
        leftIndex = rightIndex;
        rightIndex--;
        if(rightIndex < 0)
        {
            rightIndex = layers.Length - 1;
        }
    }

    void ScrollRight()
    {
        // When moving left scroll right
        layers[leftIndex].position = Vector3.right * (layers[rightIndex].position.x + backGroundSize);
        rightIndex = leftIndex;
        leftIndex++;
        if (leftIndex == layers.Length)
        {
            leftIndex = 0;
        }
    }
	
}
